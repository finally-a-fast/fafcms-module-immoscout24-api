[![Finally a fast CMS](https://www.finally-a-fast.com/logos/logo-cms-readme.jpg)](https://www.finally-a-fast.com/) | Changelog | Module Immoscout 24 API
================================================

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- CHANGELOG.md @cmoeke fafcms-core#39
- basic doc folder structure @cmoeke fafcms-core/37
- .gitlab-ci.yml for doc an build creation @cmoeke fafcms-core/38

### Changed
- composer.json to use correct dependencies @cmoeke
- LICENSE to LICENSE.md @cmoeke
- .editorconfig update @cmoeke
- .gitattributes update @cmoeke
- PHP dependency to 7.4 @cmoeke
- Changed ci config to handle new build branch @cmoeke
- Cleanup due to renaming module @StefanBrandenburger

### Fixed
- Broken README.md icons @cmoeke fafcms-core#46

[Unreleased]: https://gitlab.com/finally-a-fast/fafcms-module-immoscout24/-/tree/master
