<?php
/**
 * @author Rolf Engelmann <re@engelmann-software.de>
 * @copyright Copyright (c) 2019 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-immoscout24/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-immoscout24
 * @see https://www.finally-a-fast.com/packages/fafcms-immoscout24/docs Documentation of fafcms-immoscout24
 * @since File available since Release 1.0.0
 */

namespace fafcms\immoscout24;

use fafcms\fafcms\inputs\Checkbox;
use fafcms\fafcms\inputs\ColorInput;
use fafcms\fafcms\inputs\RadioList;
use fafcms\fafcms\inputs\Textarea;
use fafcms\fafcms\inputs\TextInput;
use Yii;
use fafcms\helpers\abstractions\PluginModule;
use fafcms\helpers\classes\PluginSetting;
use fafcms\fafcms\components\FafcmsComponent;

/**
 * Class Module
 * @package fafcms\immoscout24
 */
class Module extends PluginModule
{
    public function getPluginSettingRules(): array
    {
        return [
            [
                [
                    'active',
                ],
                'required'
            ],

            [['active', 'static', 'showLink'], 'boolean'],

        ];
    }

    protected function pluginSettingDefinitions(): array
    {
        return [
            new PluginSetting($this, [
                'name' => 'active',
                'label' => Yii::t('fafcms-immoscout24', 'active'),
                'defaultValue' => false,
                'inputType' => Checkbox::class,
                'valueType' => FafcmsComponent::VALUE_TYPE_BOOL,
            ]),
        ];
    }
}
