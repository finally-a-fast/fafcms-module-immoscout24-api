<?php
/**
 * @author Rolf Engelmann <re@engelmann-software.de>
 * @copyright Copyright (c) 2019 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-immoscout24/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-immoscout24
 * @see https://www.finally-a-fast.com/packages/fafcms-module-immoscout24/docs Documentation of fafcms-module-immoscout24
 * @since File available since Release 1.0.0
 */

namespace fafcms\immoscout24;

use Exception;
use yii\base\Application;
use fafcms\helpers\abstractions\PluginBootstrap;
use fafcms\helpers\abstractions\PluginModule;
use yii\base\BootstrapInterface;
use yii\i18n\PhpMessageSource;
use Yii;

/**
 * Class Bootstrap
 * @package fafcms\cookieconsent
 */
class Bootstrap extends PluginBootstrap implements BootstrapInterface
{
    public static $id = 'fafcms-immoscout24';

    protected function bootstrapTranslations(Application $app, PluginModule $module): bool
    {
        if (!isset($app->i18n->translations['fafcms-immoscout24'])) {
            $app->i18n->translations['fafcms-immoscout24'] = [
                'class' => PhpMessageSource::class,
                'basePath' => __DIR__ . '/messages',
                'forceTranslation' => true,
            ];
        }

        return true;
    }

    protected function bootstrapFrontendApp(Application $app, PluginModule $module): bool
    {
        try {
            if (!Yii::$app->request->isAjax && $module->getPluginSettingValue('active')) {

            }

            return true;
        } catch (Exception $e) {
            return false;
        }
    }
}
